package com.template.core.ui.widget

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SnackbarVisuals
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.template.core.ui.theme.AppColor
import com.template.core.ui.theme.AppTypography

@Composable
fun CustomSnackbar(
    text: String,
    modifier: Modifier = Modifier,
    isError: Boolean = false,
) {
    Snackbar(
        modifier = modifier.padding(16.dp),
        containerColor = if (isError) AppColor.Rose50 else AppColor.mist40,
        shape = RoundedCornerShape(8.dp),
    ) {
        Row(
            horizontalArrangement = Arrangement.spacedBy(12.dp),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Text(
                style = AppTypography.contentMediumRegular,
                text = text,
                color = Color.White,
            )
        }
    }
}

@Composable
fun CustomSnackbarHost(
    hostState: SnackbarHostState,
    modifier: Modifier = Modifier,
) {
    SnackbarHost(modifier = modifier, hostState = hostState) {
        val snackbarVisuals = it.visuals as CustomSnackbarVisuals

        CustomSnackbar(
            text = snackbarVisuals.message,
            isError = snackbarVisuals.isError,
        )
    }
}

@Stable
data class CustomSnackbarVisuals(
    override val message: String = "",
    override val duration: SnackbarDuration = SnackbarDuration.Short,
    val isError: Boolean = false,
) : SnackbarVisuals {
    override val actionLabel: String?
        get() = null
    override val withDismissAction: Boolean
        get() = false
}