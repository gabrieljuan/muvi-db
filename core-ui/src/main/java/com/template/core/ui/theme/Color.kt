/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.template.core.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

// region Text & Background
object AppColor {
    val mist10: Color = Color.White
    val mist20: Color = Color(0xFFF6F5F4)
    val mist30: Color = Color(0xFFE3E2DF)
    val mist40: Color = Color(0xFFC8C6C0)
    val mist50: Color = Color(0xFFA6A39D)
    val mist60: Color = Color(0xFF5A5852)
    val mist70: Color = Color(0xFF43423C)
    val mist80: Color = Color(0xFF302F2D)
    val mist90: Color = Color(0xFF22211F)
    val mist100: Color = Color(0xFF050504)

    val Rose50: Color = Color(0xFFDD2C00)
}
