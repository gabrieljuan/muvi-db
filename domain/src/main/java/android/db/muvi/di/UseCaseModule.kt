package android.db.muvi.di

import android.db.muvi.repository.MuviDbRepository
import android.db.muvi.usecase.FetchGenreListUseCase
import android.db.muvi.usecase.FetchMuviListUseCase
import android.db.muvi.usecase.FetchReviewListUseCase
import android.db.muvi.usecase.FetchTrailerListUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {

    @Provides
    fun provideCoroutine(): CoroutineDispatcher = Dispatchers.IO

    @Provides
    fun provideFetchMuviListUseCase(
        coroutineDispatcher: CoroutineDispatcher,
        repository: MuviDbRepository,
    ): FetchMuviListUseCase = FetchMuviListUseCase(coroutineDispatcher, repository)

    @Provides
    fun provideFetchGenreListUseCase(
        coroutineDispatcher: CoroutineDispatcher,
        repository: MuviDbRepository,
    ) : FetchGenreListUseCase = FetchGenreListUseCase(coroutineDispatcher, repository)

    @Provides
    fun provideFetchTrailerListUseCase(
        coroutineDispatcher: CoroutineDispatcher,
        repository: MuviDbRepository,
    ) : FetchTrailerListUseCase = FetchTrailerListUseCase(coroutineDispatcher, repository)

    @Provides
    fun provideFetchReviewListUseCase(
        coroutineDispatcher: CoroutineDispatcher,
        repository: MuviDbRepository,
    ) : FetchReviewListUseCase = FetchReviewListUseCase(coroutineDispatcher, repository)
}
