package android.db.muvi.usecase

import android.db.muvi.core.base.ActionResult
import android.db.muvi.core.base.BaseUseCase
import android.db.muvi.core.model.DataResult
import android.db.muvi.model.MuviTrailer
import android.db.muvi.model.MuviTrailerList
import android.db.muvi.repository.MuviDbRepository
import kotlinx.coroutines.CoroutineDispatcher

class FetchTrailerListUseCase(
    coroutineDispatcher: CoroutineDispatcher,
    private val repository: MuviDbRepository,
) : BaseUseCase<FetchTrailerListUseCase.Param, MuviTrailerList, FetchTrailerListActionResult>(
    coroutineDispatcher
) {

    data class Param(val muviId: Long)

    override suspend fun execute(param: Param?): DataResult<MuviTrailerList> {
        if (param == null) throw IllegalArgumentException("Param cannot be null")
        return repository.fetchMuviTrailer(param.muviId)
    }

    override suspend fun DataResult<MuviTrailerList>.transformToUseCaseResult(): FetchTrailerListActionResult =
        when (this) {
            is DataResult.Success -> if (this.value.trailerList.isEmpty()) {
                FetchTrailerListActionResult.Failed
            } else {
                FetchTrailerListActionResult.Success(this.value.trailerList)
            }

            is DataResult.Exception -> FetchTrailerListActionResult.Failed
        }
}

sealed interface FetchTrailerListActionResult : ActionResult {
    data class Success(val trailerList: List<MuviTrailer>) : FetchTrailerListActionResult

    object Failed : FetchTrailerListActionResult
}