package android.db.muvi.usecase

import android.db.muvi.core.base.ActionResult
import android.db.muvi.core.base.BaseUseCase
import android.db.muvi.core.model.DataResult
import android.db.muvi.model.Genre
import android.db.muvi.model.GenreList
import android.db.muvi.repository.MuviDbRepository
import kotlinx.coroutines.CoroutineDispatcher

class FetchGenreListUseCase(
    coroutineDispatcher: CoroutineDispatcher,
    private val repository: MuviDbRepository,
) : BaseUseCase<FetchGenreListUseCase.Params, GenreList, FetchGenreListActionResult>(
    coroutineDispatcher
) {

    data class Params(val language: String)

    override suspend fun execute(param: Params?): DataResult<GenreList> {
        if (param == null) throw IllegalArgumentException("Param cannot be null")
        return repository.fetchMuviGenre(param.language)
    }

    override suspend fun DataResult<GenreList>.transformToUseCaseResult(): FetchGenreListActionResult =
        when (this) {
            is DataResult.Success -> {
                if (value.genreList.isNotEmpty())
                    FetchGenreListActionResult.Success(value.genreList)
                else
                    FetchGenreListActionResult.Empty
            }

            is DataResult.Exception -> FetchGenreListActionResult.Failed
        }
}

sealed interface FetchGenreListActionResult : ActionResult {
    data class Success(val genreList: List<Genre>) : FetchGenreListActionResult
    object Failed : FetchGenreListActionResult
    object Empty : FetchGenreListActionResult
}