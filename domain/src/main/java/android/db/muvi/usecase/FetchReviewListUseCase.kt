package android.db.muvi.usecase

import android.db.muvi.core.base.ActionResult
import android.db.muvi.core.base.BaseUseCase
import android.db.muvi.core.model.DataResult
import android.db.muvi.model.Review
import android.db.muvi.model.ReviewList
import android.db.muvi.repository.MuviDbRepository
import kotlinx.coroutines.CoroutineDispatcher

class FetchReviewListUseCase(
    coroutineDispatcher: CoroutineDispatcher,
    private val repository: MuviDbRepository,
) : BaseUseCase<FetchReviewListUseCase.Param, ReviewList, FetchReviewListActionResult>(
    coroutineDispatcher
) {

    data class Param(val muviId: Long)

    override suspend fun execute(param: Param?): DataResult<ReviewList> {
        if (param == null) throw IllegalArgumentException("Param cannot be null")
        return repository.fetchReview(param.muviId)
    }

    override suspend fun DataResult<ReviewList>.transformToUseCaseResult(): FetchReviewListActionResult =
        when (this) {
            is DataResult.Success -> if (value.reviewList.isEmpty()) {
                FetchReviewListActionResult.Failed
            } else {
                FetchReviewListActionResult.Success(value.reviewList)
            }

            is DataResult.Exception -> FetchReviewListActionResult.Failed
        }
}

sealed interface FetchReviewListActionResult : ActionResult {
    data class Success(val reviewList: List<Review>) : FetchReviewListActionResult
    object Failed : FetchReviewListActionResult
}