package android.db.muvi.usecase

import android.db.muvi.core.base.ActionResult
import android.db.muvi.core.base.BaseUseCase
import android.db.muvi.core.model.DataResult
import android.db.muvi.model.Muvi
import android.db.muvi.model.MuviList
import android.db.muvi.repository.MuviDbRepository
import kotlinx.coroutines.CoroutineDispatcher

class FetchMuviListUseCase(
    coroutineDispatcher: CoroutineDispatcher,
    private val repository: MuviDbRepository,
) : BaseUseCase<FetchMuviListUseCase.Param, MuviList, FetchMuviListUseCaseResult>(coroutineDispatcher) {
    override suspend fun execute(param: Param?): DataResult<MuviList> {
        if(param == null) throw IllegalArgumentException("Param cannot be null")
        return repository.fetchMuviList(param.genre, param.page.toString())
    }


    override suspend fun DataResult<MuviList>.transformToUseCaseResult(): FetchMuviListUseCaseResult =
        when (this) {
            is DataResult.Success -> {
                if (value.muviList.isNotEmpty()) {
                    FetchMuviListUseCaseResult.Success(value.muviList)
                } else {
                    FetchMuviListUseCaseResult.Empty
                }
            }

            is DataResult.Exception -> FetchMuviListUseCaseResult.Failed
        }

    data class Param(
        val genre: String,
        val page: Int,
    )
}

sealed interface FetchMuviListUseCaseResult : ActionResult {
    data class Success(val muviList: List<Muvi>) : FetchMuviListUseCaseResult
    object Empty : FetchMuviListUseCaseResult
    object Failed : FetchMuviListUseCaseResult
}
