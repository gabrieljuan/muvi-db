package android.db.muvi

import android.template.feature.mymodel.muvilist.mymodel.FakeMyModelRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@OptIn(ExperimentalCoroutinesApi::class) // TODO: Remove when stable
class MyModelViewModelTest {
    @Test
    fun uiState_initiallyLoading() = runTest {
        val viewModel = MyModelViewModel(FakeMyModelRepository())
        assertEquals(viewModel.uiState.first(), MyModelUiState.Loading)
    }

    @Test
    fun uiState_onItemSaved_isDisplayed() = runTest {
        val viewModel = MyModelViewModel(FakeMyModelRepository())
        assertEquals(viewModel.uiState.first(), MyModelUiState.Loading)
    }
}