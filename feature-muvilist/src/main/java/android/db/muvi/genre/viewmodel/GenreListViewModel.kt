package android.db.muvi.genre.viewmodel

import android.db.muvi.core.base.ActionResult
import android.db.muvi.core.base.BaseViewModel
import android.db.muvi.usecase.FetchGenreListActionResult
import android.db.muvi.usecase.FetchGenreListUseCase
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class GenreListViewModel @Inject constructor(
    private val genreListUseCase: FetchGenreListUseCase,
) : BaseViewModel<GenreListViewState, GenreListViewAction>(GenreListViewState()) {

    override fun renderViewState(result: ActionResult?): GenreListViewState =
        when (result) {
            GenreListActionResult.ShowLoading -> getCurrentViewState().copy(isLoading = true)
            is FetchGenreListActionResult -> result.mapFetchGenreListActionResult()
            else -> getCurrentViewState()
        }

    override fun handleAction(action: GenreListViewAction): LiveData<ActionResult> =
        liveData(viewModelDispatcher) {
            when (action) {
                is GenreListViewAction.Init -> {
                    emit(GenreListActionResult.ShowLoading)
                    emit(
                        genreListUseCase.getResult(FetchGenreListUseCase.Params(action.language))
                    )
                }

                is GenreListViewAction.OnGenreClick -> TODO()
            }
        }

    private fun FetchGenreListActionResult.mapFetchGenreListActionResult(): GenreListViewState =
        when (this) {
            is FetchGenreListActionResult.Success -> getCurrentViewState().copy(
                genreList = genreList,
                isLoading = false,
            )

            is FetchGenreListActionResult.Failed -> getCurrentViewState().copy(
                errorMessage = "Failed to fetch genre",
                isLoading = false,
            )

            is FetchGenreListActionResult.Empty -> getCurrentViewState().copy(
                genreList = emptyList(),
                isLoading = false,
            )
        }

}