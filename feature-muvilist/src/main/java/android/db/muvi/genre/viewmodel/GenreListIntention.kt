package android.db.muvi.genre.viewmodel

import android.db.muvi.core.base.ActionResult
import android.db.muvi.core.base.ViewAction
import android.db.muvi.core.base.ViewState
import android.db.muvi.model.Genre

data class GenreListViewState(
    val genreList: List<Genre> = emptyList(),
    val isLoading: Boolean = false,
    val errorMessage: String = "",
): ViewState

sealed interface GenreListViewAction : ViewAction {
    data class Init(val language: String): GenreListViewAction
    data class OnGenreClick(val genreId: Long): GenreListViewAction
}

sealed interface GenreListActionResult : ActionResult {
    object ShowLoading: GenreListActionResult
}