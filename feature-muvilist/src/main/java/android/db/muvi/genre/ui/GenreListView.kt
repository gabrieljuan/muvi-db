@file:OptIn(ExperimentalMaterial3Api::class)

package android.db.muvi.genre.ui

import android.db.muvi.R
import android.db.muvi.genre.viewmodel.GenreListViewAction
import android.db.muvi.genre.viewmodel.GenreListViewModel
import android.db.muvi.genre.viewmodel.GenreListViewState
import android.db.muvi.model.Genre
import android.db.muvi.navigation.MuviListNavigationRoute
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.core.os.bundleOf
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.template.core.ui.navigation.navigate
import com.template.core.ui.theme.AppColor
import com.template.core.ui.widget.AppBar
import com.template.core.ui.widget.CustomSnackbarHost
import com.template.core.ui.widget.CustomSnackbarVisuals

@Composable
fun GenreListRoute(
    navController: NavController,
    modifier: Modifier = Modifier,
) {
    val viewModel: GenreListViewModel = hiltViewModel()
    val viewState: GenreListViewState by viewModel.viewState.observeAsState(GenreListViewState())

    LaunchedEffect(Unit) {
        viewModel.onAction(GenreListViewAction.Init("en"))
    }

    GenreListScreen(
        modifier = modifier,
        genreList = viewState.genreList,
        errorMessage = viewState.errorMessage,
        isLoading = viewState.isLoading,
        onGenreClick = {
            val args = bundleOf(MuviListNavigationRoute.MUVI_LIST_GENRE_ARGS to it)
            navController.navigate(
                route = MuviListNavigationRoute.MUVI_LIST_ROUTE,
                args = args,
            )
        },
    )
}

@Composable
private fun GenreListScreen(
    genreList: List<Genre>,
    errorMessage: String,
    isLoading: Boolean,
    modifier: Modifier = Modifier,
    onGenreClick: (String) -> Unit,
    listState: LazyListState = rememberLazyListState(),
) {
    val snackBarHostState = remember { SnackbarHostState() }
    LaunchedEffect(errorMessage) {
        if (errorMessage.isNotEmpty()) {
            snackBarHostState.showSnackbar(
                CustomSnackbarVisuals(
                    message = errorMessage,
                    isError = true,
                )
            )
        }
    }
    Scaffold(
        containerColor = AppColor.mist20,
        topBar = {
            AppBar(
                title = stringResource(id = R.string.genre_list_title),
            )
        },
        snackbarHost = { CustomSnackbarHost(hostState = snackBarHostState) },
    ) {
        Box(
            modifier = modifier.padding(it)
        ) {
            LazyColumn(
                modifier = Modifier
                    .padding(vertical = 8.dp)
                    .fillMaxWidth(),
                state = listState,
            ) {
                items(items = genreList) { genre ->
                    GenreCardItem(
                        genreName = genre.name,
                        onCardClick = { onGenreClick(genre.id.toString()) },
                    )
                }
            }
            if (isLoading) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .align(Alignment.Center)
                )
            }
        }
    }
}