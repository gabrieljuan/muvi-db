@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class)

package android.db.muvi.list.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.ColorPainter
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.template.core.ui.theme.AppColor
import com.template.core.ui.theme.AppTypography

@Composable
fun MuviCardItem(
    muviName: String,
    muviReleaseDate: String,
    muviPosterURL: String,
    modifier: Modifier = Modifier,
    onCardClick: () -> Unit,
) {
    Card(
        modifier = modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp, vertical = 4.dp),
        shape = RoundedCornerShape(8.dp),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 8.dp
        ),
        colors = CardDefaults.cardColors(
            containerColor = Color.White,
        ),
        onClick = onCardClick,
    ) {
        Column(
            modifier = Modifier.padding(
                bottom = 8.dp,
            )
        ) {
            AsyncImage(
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(0.6f),
                model = muviPosterURL,
                contentDescription = "movie poster",
                placeholder = ColorPainter(AppColor.mist40),
                error = ColorPainter(AppColor.mist40),
            )
            Text(
                modifier = Modifier.padding(
                    top = 8.dp,
                    start = 16.dp,
                    end = 16.dp,
                ),
                text = muviName,
                color = Color.Black,
                style = AppTypography.contentMediumBold,
            )
            Text(
                modifier = Modifier.padding(
                    top = 8.dp,
                    start = 16.dp,
                    end = 16.dp,
                ),
                text = muviReleaseDate,
                color = AppColor.mist80,
                style = AppTypography.contentMediumRegular,
            )
        }
    }
}
