package android.db.muvi.list.viewmodel

import android.db.muvi.core.base.ActionResult
import android.db.muvi.core.base.ViewAction
import android.db.muvi.core.base.ViewState
import android.db.muvi.model.Muvi

data class MuviListViewState(
    val isLoading: Boolean = false,
    val muviList: List<Muvi> = emptyList(),
    val errorMessage: String = "",
) : ViewState

sealed interface MuviListViewAction : ViewAction {
    data class Init(val genre: String) : MuviListViewAction

    object LoadMore: MuviListViewAction
}

sealed interface MuviListActionResult : ActionResult {
    object ShowLoading : MuviListActionResult
}
