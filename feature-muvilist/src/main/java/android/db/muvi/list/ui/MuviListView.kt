@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class)

package android.db.muvi.list.ui

import android.db.muvi.R
import android.db.muvi.list.viewmodel.MuviListViewAction
import android.db.muvi.list.viewmodel.MuviListViewModel
import android.db.muvi.list.viewmodel.MuviListViewState
import android.db.muvi.model.Muvi
import android.db.muvi.navigation.MuviListNavigationRoute
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.core.os.bundleOf
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.template.core.ui.navigation.navigate
import com.template.core.ui.theme.AppColor
import com.template.core.ui.widget.AppBar
import com.template.core.ui.widget.CustomSnackbarHost
import com.template.core.ui.widget.CustomSnackbarVisuals

@Composable
fun MuviListRoute(
    genreId: String,
    navController: NavController,
    modifier: Modifier = Modifier,
) {
    val viewModel: MuviListViewModel = hiltViewModel()
    val viewState: MuviListViewState by viewModel.viewState.observeAsState(initial = MuviListViewState())

    LaunchedEffect(Unit) {
        viewModel.onAction(MuviListViewAction.Init(genreId))
    }

    MuviListScreen(
        modifier = modifier,
        muviList = viewState.muviList,
        errorMessage = viewState.errorMessage,
        isLoading = viewState.isLoading,
        onMuviClick = {
            navController.navigate(
                route = MuviListNavigationRoute.MUVI_DETAIL_ROUTE,
                args = bundleOf(
                    MuviListNavigationRoute.MUVI_DETAIL_MUVI_ARGS to it
                )
            )
        },
        onLoadMoreItems = {
            viewModel.onAction(MuviListViewAction.LoadMore)
        },
        onBackClick = {
            navController.navigateUp()
        }
    )
}

@Composable
private fun MuviListScreen(
    muviList: List<Muvi>,
    errorMessage: String,
    isLoading: Boolean,
    modifier: Modifier = Modifier,
    onMuviClick: (Muvi) -> Unit,
    onLoadMoreItems: () -> Unit,
    onBackClick: () -> Unit,
    listState: LazyListState = rememberLazyListState(),
) {
    val snackBarHostState = remember { SnackbarHostState() }
    LaunchedEffect(errorMessage) {
        if (errorMessage.isNotEmpty()) {
            snackBarHostState.showSnackbar(
                CustomSnackbarVisuals(
                    message = errorMessage,
                    isError = true,
                )
            )
        }
    }
    Scaffold(
        containerColor = AppColor.mist20,
        topBar = {
            AppBar(
                title = stringResource(id = R.string.muvi_list_title),
                navigationIcon = R.drawable.ic_back,
                onClickNavigationIcon = onBackClick,
            )
        },
        snackbarHost = { CustomSnackbarHost(hostState = snackBarHostState) }
    ) {
        Box(
            modifier = modifier.padding(it)
        ) {
            LazyColumn(
                modifier = Modifier
                    .padding(vertical = 8.dp)
                    .fillMaxWidth(),
                state = listState,
            ) {
                items(items = muviList) { muvi ->
                    if (muviList.last() == muvi) {
                        onLoadMoreItems()
                    }
                    MuviCardItem(
                        muviName = muvi.title,
                        muviReleaseDate = muvi.releaseDate,
                        muviPosterURL = muvi.fullPosterURL,
                        onCardClick = { onMuviClick(muvi) },
                    )
                }
            }
            if (isLoading) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .align(Alignment.Center)
                )
            }
        }
    }
}