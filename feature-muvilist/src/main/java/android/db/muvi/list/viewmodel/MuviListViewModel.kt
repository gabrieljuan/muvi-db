package android.db.muvi.list.viewmodel

import android.db.muvi.core.base.ActionResult
import android.db.muvi.core.base.BaseViewModel
import android.db.muvi.model.Muvi
import android.db.muvi.usecase.FetchMuviListUseCase
import android.db.muvi.usecase.FetchMuviListUseCaseResult
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MuviListViewModel @Inject constructor(
    private val fetchMuviListUseCase: FetchMuviListUseCase,
) : BaseViewModel<MuviListViewState, MuviListViewAction>(MuviListViewState()) {

    private val muviList = mutableListOf<Muvi>()
    private var currentIndex = 1
    private var genre = ""

    override fun renderViewState(result: ActionResult?): MuviListViewState =
        when (result) {
            MuviListActionResult.ShowLoading -> getCurrentViewState().copy(isLoading = true)
            is FetchMuviListUseCaseResult -> result.mapFetchMuviListResult()
            else -> getCurrentViewState()
        }

    override fun handleAction(action: MuviListViewAction): LiveData<ActionResult> =
        liveData(viewModelDispatcher) {
            when (action) {
                is MuviListViewAction.Init -> {
                    genre = action.genre
                    emit(MuviListActionResult.ShowLoading)
                    emit(
                        fetchMuviListUseCase.getResult(
                            FetchMuviListUseCase.Param(genre, currentIndex)
                        )
                    )
                }

                is MuviListViewAction.LoadMore -> {
                    emit(MuviListActionResult.ShowLoading)
                    emit(
                        fetchMuviListUseCase.getResult(
                            FetchMuviListUseCase.Param(genre, ++currentIndex)
                        )
                    )
                }
            }
        }

    private fun FetchMuviListUseCaseResult.mapFetchMuviListResult(): MuviListViewState =
        when (this) {
            is FetchMuviListUseCaseResult.Success -> {
                this@MuviListViewModel.muviList.addAll(muviList)
                getCurrentViewState().copy(
                    muviList = this@MuviListViewModel.muviList,
                    isLoading = false,
                )
            }

            is FetchMuviListUseCaseResult.Failed -> getCurrentViewState().copy(
                errorMessage = "Failed to fetch muvi list",
                isLoading = false,
            )

            else -> getCurrentViewState()
        }

}
