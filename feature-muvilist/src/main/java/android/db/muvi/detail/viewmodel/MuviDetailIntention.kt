package android.db.muvi.detail.viewmodel

import android.db.muvi.core.base.ActionResult
import android.db.muvi.core.base.ViewAction
import android.db.muvi.core.base.ViewState
import android.db.muvi.model.Muvi
import android.db.muvi.model.MuviTrailer
import android.db.muvi.model.Review

data class MuviDetailViewState(
    val muvi: Muvi = Muvi(),
    val trailer: MuviTrailer? = null,
    val reviewList: List<Review> = listOf(),
) : ViewState

sealed interface MuviDetailViewAction : ViewAction {
    data class Init(val muvi: Muvi) : MuviDetailViewAction
}

sealed interface MuviDetailActionResult : ActionResult {
    data class OnMuviLoaded(
        val muvi: Muvi,
        val reviewList: List<Review>,
        val trailer: MuviTrailer?,
    ) : MuviDetailActionResult
}
