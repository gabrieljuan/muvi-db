@file:OptIn(ExperimentalMaterial3Api::class)

package android.db.muvi.detail.ui

import android.db.muvi.R
import android.db.muvi.detail.viewmodel.MuviDetailViewAction
import android.db.muvi.detail.viewmodel.MuviDetailViewModel
import android.db.muvi.detail.viewmodel.MuviDetailViewState
import android.db.muvi.model.Muvi
import android.db.muvi.model.MuviTrailer
import android.db.muvi.model.Review
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.template.core.ui.theme.AppColor
import com.template.core.ui.theme.AppTypography
import com.template.core.ui.widget.AppBar

@Composable
fun MuviDetailRoute(
    muvi: Muvi,
    navController: NavController,
    modifier: Modifier = Modifier,
) {
    val viewModel: MuviDetailViewModel = hiltViewModel()
    val viewState: MuviDetailViewState by viewModel.viewState.observeAsState(initial = MuviDetailViewState())

    LaunchedEffect(Unit) {
        viewModel.onAction(MuviDetailViewAction.Init(muvi))
    }

    MuviDetailScreen(
        modifier = modifier,
        muvi = viewState.muvi,
        trailer = viewState.trailer,
        reviewList = viewState.reviewList,
        onBackClick = {
            navController.navigateUp()
        }
    )
}

@Composable
private fun MuviDetailScreen(
    muvi: Muvi,
    trailer: MuviTrailer?,
    reviewList: List<Review>,
    onBackClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    Scaffold(
        containerColor = AppColor.mist20,
        topBar = {
            AppBar(
                title = muvi.title,
                navigationIcon = R.drawable.ic_back,
                onClickNavigationIcon = onBackClick,
            )
        }
    ) {
        LazyColumn(
            modifier = modifier
                .fillMaxSize()
                .padding(it),
            state = rememberLazyListState(),
        ) {
            item {
                AsyncImage(
                    modifier = Modifier
                        .fillMaxWidth()
                        .aspectRatio(1.778f),
                    model = muvi.fullBackdropURL,
                    contentDescription = "muvi backdrop",
                )
            }
            item {
                Text(
                    modifier = Modifier.padding(
                        horizontal = 16.dp,
                        vertical = 8.dp,
                    ),
                    text = muvi.title,
                    style = AppTypography.titleMediumBold,
                    color = Color.Black,
                )
            }
            item {
                Text(
                    modifier = Modifier.padding(
                        horizontal = 16.dp,
                    ),
                    text = muvi.releaseDate,
                    style = AppTypography.contentMediumRegular,
                    color = AppColor.mist80,
                )
            }
            item {
                Text(
                    modifier = Modifier.padding(
                        start = 16.dp,
                        end = 16.dp,
                        top = 8.dp,
                    ),
                    text = "Rating : ${muvi.voteAverage} / 10",
                    style = AppTypography.contentMediumRegular,
                    color = AppColor.mist80,
                )
            }
            item {
                Text(
                    modifier = Modifier.padding(
                        start = 16.dp,
                        end = 16.dp,
                        top = 8.dp,
                    ),
                    text = muvi.overview,
                    style = AppTypography.contentSmallRegular,
                    color = AppColor.mist100,
                )
            }
            if (trailer != null) {
                item {
                    YoutubePlayerView(
                        modifier = Modifier.padding(
                            start = 16.dp,
                            end = 16.dp,
                            top = 8.dp,
                        ),
                        videoId = trailer.key,
                    )
                }
            }
            if (reviewList.isNotEmpty()) {
                item {
                    Text(
                        modifier = Modifier.padding(
                            horizontal = 16.dp,
                            vertical = 8.dp,
                        ),
                        text = stringResource(id = R.string.review_label),
                        style = AppTypography.titleMediumBold,
                        color = AppColor.mist100,
                    )
                }
                items(reviewList) {
                    Column(
                        modifier = Modifier.padding(
                            start = 16.dp,
                            end = 16.dp,
                            top = 8.dp,
                        )
                    ) {
                        Text(
                            modifier = Modifier.padding(
                                top = 8.dp,
                            ),
                            text = it.author,
                            style = AppTypography.titleSmallRegular,
                            color = AppColor.mist100,
                        )
                        Text(
                            modifier = Modifier.padding(
                                top = 8.dp,
                            ),
                            text = it.content,
                            style = AppTypography.contentSmallRegular,
                            color = AppColor.mist70,
                        )
                    }
                }
            }
        }
    }
}