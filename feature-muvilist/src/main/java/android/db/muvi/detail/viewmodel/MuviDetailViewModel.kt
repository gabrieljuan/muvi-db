package android.db.muvi.detail.viewmodel

import android.db.muvi.core.base.ActionResult
import android.db.muvi.core.base.BaseViewModel
import android.db.muvi.usecase.FetchReviewListActionResult
import android.db.muvi.usecase.FetchReviewListUseCase
import android.db.muvi.usecase.FetchTrailerListActionResult
import android.db.muvi.usecase.FetchTrailerListUseCase
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MuviDetailViewModel @Inject constructor(
    private val fetchTrailerListUseCase: FetchTrailerListUseCase,
    private val fetchReviewListUseCase: FetchReviewListUseCase,
) : BaseViewModel<MuviDetailViewState, MuviDetailViewAction>(MuviDetailViewState()) {
    override fun renderViewState(result: ActionResult?): MuviDetailViewState =
        when (result) {
            is MuviDetailActionResult.OnMuviLoaded -> getCurrentViewState().copy(
                muvi = result.muvi,
                trailer = result.trailer,
                reviewList = result.reviewList
            )
            else -> getCurrentViewState()
        }

    override fun handleAction(action: MuviDetailViewAction): LiveData<ActionResult> =
        liveData(viewModelDispatcher) {
            when (action) {
                is MuviDetailViewAction.Init -> {
                    val trailer =
                        (fetchTrailerListUseCase.getResult(FetchTrailerListUseCase.Param(action.muvi.id))
                                as? FetchTrailerListActionResult.Success)?.trailerList?.first()
                    val reviewList =
                        (fetchReviewListUseCase.getResult(FetchReviewListUseCase.Param(action.muvi.id))
                                as? FetchReviewListActionResult.Success)?.reviewList.orEmpty()

                    emit(MuviDetailActionResult.OnMuviLoaded(action.muvi, reviewList, trailer))
                }
            }
        }

}
