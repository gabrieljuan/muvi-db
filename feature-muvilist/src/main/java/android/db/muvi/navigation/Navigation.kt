package android.db.muvi.navigation

import android.db.muvi.detail.ui.MuviDetailRoute
import android.db.muvi.genre.ui.GenreListRoute
import android.db.muvi.list.ui.MuviListRoute
import android.db.muvi.model.Muvi
import android.db.muvi.navigation.MuviListNavigationRoute.MUVI_DETAIL_MUVI_ARGS
import android.db.muvi.navigation.MuviListNavigationRoute.MUVI_DETAIL_ROUTE
import android.db.muvi.navigation.MuviListNavigationRoute.MUVI_GENRE_LIST_ROUTE
import android.db.muvi.navigation.MuviListNavigationRoute.MUVI_LIST_GENRE_ARGS
import android.db.muvi.navigation.MuviListNavigationRoute.MUVI_LIST_NAV_GRAPH_ROUTE
import android.db.muvi.navigation.MuviListNavigationRoute.MUVI_LIST_ROUTE
import android.os.Build
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import androidx.navigation.navigation


fun NavGraphBuilder.muviListNavGraph(navController: NavController) {
    navigation(
        startDestination = MUVI_GENRE_LIST_ROUTE,
        route = MUVI_LIST_NAV_GRAPH_ROUTE,
    ){
        composable(route = MUVI_GENRE_LIST_ROUTE) {
            GenreListRoute(navController = navController)
        }
        composable(route = MUVI_LIST_ROUTE) {backStackEntry ->
            val genreId = backStackEntry.arguments?.getString(MUVI_LIST_GENRE_ARGS).orEmpty()
            MuviListRoute(genreId = genreId, navController = navController)
        }
        composable(route = MUVI_DETAIL_ROUTE) {backStackEntry ->
            val muvi: Muvi = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                backStackEntry.arguments?.getParcelable(MUVI_DETAIL_MUVI_ARGS, Muvi::class.java)
            } else {
                backStackEntry.arguments?.getParcelable(MUVI_DETAIL_MUVI_ARGS)
            } ?: Muvi()
            MuviDetailRoute(muvi = muvi, navController = navController)
        }
    }
}
object MuviListNavigationRoute{
    const val MUVI_LIST_NAV_GRAPH_ROUTE = "muviListNavGraphRoute"
    const val MUVI_GENRE_LIST_ROUTE = "muviGenreListRoute"
    const val MUVI_LIST_ROUTE = "muviListRoute"
    const val MUVI_LIST_GENRE_ARGS = "muviListGenreArgs"
    const val MUVI_DETAIL_ROUTE = "muviDetailRoute"
    const val MUVI_DETAIL_MUVI_ARGS = "muviDetailMuviArgs"
}