Muvi DB (Personal Project)
==================

This project should be run in latest Android Development Environment

![Screenshot](https://gitlab.com/gabrieljuan/muvi-db/-/raw/master/genre_list.png)
![Screenshot](https://gitlab.com/gabrieljuan/muvi-db/-/raw/master/muvi_list.png)
![Screenshot](https://gitlab.com/gabrieljuan/muvi-db/-/raw/master/muvi_detail.png)

## Features

* Hilt Dependency Injection
* MVVM / MVI
* UI in Compose, infinite list, youtube player
* Navigation
* Repository and data source
* Kotlin Coroutines and LiveData
* 
# License

Now in Android is distributed under the terms of the Apache License (Version 2.0). See the
[license](LICENSE) for more information.
