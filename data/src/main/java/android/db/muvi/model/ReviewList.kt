package android.db.muvi.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ReviewList(
    val id: Long,
    @SerialName("results") val reviewList: List<Review>,
)
