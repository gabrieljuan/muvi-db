package android.db.muvi.model

import kotlinx.serialization.Serializable

@Serializable
data class MuviTrailer(
    val key: String,
    val id : String,
)