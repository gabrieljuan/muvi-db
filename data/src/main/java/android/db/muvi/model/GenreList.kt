package android.db.muvi.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class GenreList(
    @SerialName("genres") val genreList: List<Genre>
)