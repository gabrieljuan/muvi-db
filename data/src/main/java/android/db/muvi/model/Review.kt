package android.db.muvi.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Review(
    val author: String,
    val content: String,
    @SerialName("updated_at") val lastUpdateDate: String,
)