package android.db.muvi.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MuviList(
    val page: Int,
    @SerialName("results") val muviList: List<Muvi>,
)