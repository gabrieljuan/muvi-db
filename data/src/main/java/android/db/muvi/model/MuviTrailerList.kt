package android.db.muvi.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MuviTrailerList(
    val id: Long,
    @SerialName("results") val trailerList: List<MuviTrailer>
)
