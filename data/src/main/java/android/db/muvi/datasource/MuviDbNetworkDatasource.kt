package android.db.muvi.datasource

import android.db.muvi.constant.MuviDbApiQueryParam
import android.db.muvi.constant.MuviDbApiUrl
import android.db.muvi.core.model.DataResult
import android.db.muvi.model.Genre
import android.db.muvi.model.GenreList
import android.db.muvi.model.Muvi
import android.db.muvi.model.MuviList
import android.db.muvi.model.MuviTrailerList
import android.db.muvi.model.ReviewList
import android.db.muvi.repository.MuviDbRepository
import android.db.muvi.util.tryGetDataCall
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.engine.android.Android
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import javax.inject.Inject

class MuviDbNetworkDatasource @Inject constructor(
    private val httpClient: HttpClient
) : MuviDbRepository {
    override suspend fun fetchMuviList(genre: String, page: String): DataResult<MuviList> = tryGetDataCall {
        httpClient.get(urlString = MuviDbApiUrl.MUVI_DISCOVER_LIST_API_URL) {
            parameter(MuviDbApiQueryParam.MUVI_DISCOVER_LIST_GENRE_ID, genre)
            parameter(MuviDbApiQueryParam.MUVI_DISCOVER_LIST_PAGE_NUMBER, page)
        }.body()
    }

    override suspend fun fetchMuviGenre(language: String): DataResult<GenreList> = tryGetDataCall {
            httpClient.get(urlString = MuviDbApiUrl.MUVI_GENRE_LIST_API_URL) {
                parameter(MuviDbApiQueryParam.MUVI_GENRE_LANGUAGE_ID, language)
            }.body()
        }

    override suspend fun fetchMuviTrailer(muviId: Long): DataResult<MuviTrailerList> = tryGetDataCall {
        httpClient.get(urlString = MuviDbApiUrl.getMuviTrailerUrl(muviId)).body()
    }

    override suspend fun fetchReview(muviId: Long): DataResult<ReviewList> = tryGetDataCall {
        httpClient.get(urlString = MuviDbApiUrl.getMuviReviews(muviId)).body()
    }
}
