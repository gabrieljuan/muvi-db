package android.db.muvi.constant

object MuviDbApiUrl {
    const val MUVI_DISCOVER_LIST_API_URL = "https://api.themoviedb.org/3/discover/movie"
    const val MUVI_GENRE_LIST_API_URL = "https://api.themoviedb.org/3/genre/movie/list"
    fun getMuviTrailerUrl(muviId: Long): String = "https://api.themoviedb.org/3/movie/$muviId/videos"
    fun getMuviReviews(muviId: Long): String = "https://api.themoviedb.org/3/movie/$muviId/reviews"
}

object MuviDbApiQueryParam {
    const val MUVI_DISCOVER_LIST_GENRE_ID = "with_genres"
    const val MUVI_DISCOVER_LIST_PAGE_NUMBER = "page"
    const val MUVI_GENRE_LANGUAGE_ID = "language"
}

object MuviDbApiAuthorization {
    const val API_BEARER_AUTH = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5MWZjODkzNjBkYjZhZGViYjJkZDBkYWFjZmNhMDFmNiIsInN1YiI6IjY1MWQ1N2JiOGMyMmMwMDEyMDU0YWNkMyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.GPWzlFwsRyobO-5z4hV9P8RUAcYeAPk8e0asYsYp2o4"
}
