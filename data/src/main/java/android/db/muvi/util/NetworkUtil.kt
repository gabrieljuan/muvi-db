package android.db.muvi.util

import android.db.muvi.core.model.DataResult

inline fun <T : Any> tryGetDataCall(dataCall: () -> T): DataResult<T> = try {
    val value = dataCall()
    DataResult.Success(value)
} catch (ex: Throwable) {
    DataResult.Exception(ex)
}
