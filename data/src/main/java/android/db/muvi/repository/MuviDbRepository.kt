package android.db.muvi.repository

import android.db.muvi.core.model.DataResult
import android.db.muvi.model.Genre
import android.db.muvi.model.GenreList
import android.db.muvi.model.Muvi
import android.db.muvi.model.MuviList
import android.db.muvi.model.MuviTrailerList
import android.db.muvi.model.ReviewList

interface MuviDbRepository {
    suspend fun fetchMuviList(genre: String, page: String) : DataResult<MuviList>

    suspend fun fetchMuviGenre(language: String) : DataResult<GenreList>

    suspend fun fetchMuviTrailer(muviId: Long): DataResult<MuviTrailerList>

    suspend fun fetchReview(muviId: Long): DataResult<ReviewList>
}
